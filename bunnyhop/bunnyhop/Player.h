#pragma once
#include "AnimatingObject.h"
#include <SFML/Audio.hpp>
class Player : public AnimatingObjects
{
public:
	Player(sf::Vector2u screenSize);

	void Input(sf::Vector2u screenSize);
	void Update(sf::Time frameTime, sf::Vector2u screenSize);
	bool HandleSolidCollision(sf::FloatRect otherHitbox);
	sf::FloatRect GetHitBox();
	bool GetAlive();
private:
	// Data
	sf::Vector2f velocity;
	float speed;
	float gravity;
	sf::Vector2f previousPosition;
	bool isCrouching;
	bool isAlive;
	sf::SoundBuffer deadBuffer;
	sf::Sound gameDead;
};
