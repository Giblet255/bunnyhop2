#include "Game.h"

Game::Game()
	: window(sf::VideoMode::getDesktopMode(), "DinoRun", sf::Style::Titlebar | sf::Style::Close)
	, gameClock()
	, levelScreenInstance(this)
{
	bgTexture.loadFromFile("Assets/Graphics/Background.png");
	Background.setTexture(bgTexture);
	Background.setPosition(0, 0);
	
	// Window setup
	window.setMouseCursorVisible(true);
}

void Game::RunGameLoop()

{
	// Repeat as long as the window is open
	while (window.isOpen())
	{
		Input();
		Update();
		Draw();
	}

}

void Game::Input()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
		// Close game if escape is pressed
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			window.close();
		}
	}

	levelScreenInstance.Input();
}

void Game::Update()
{
	sf::Time frameTime = gameClock.restart();

	levelScreenInstance.Update(frameTime, window.getSize());
}

void Game::Draw()
{
	
	
	window.clear(sf::Color(255, 128, 0));
	window.draw(Background);
	levelScreenInstance.DrawTo(window);
	window.display();
}

sf::RenderWindow& Game::GetWindow()
{
	return window;
}
