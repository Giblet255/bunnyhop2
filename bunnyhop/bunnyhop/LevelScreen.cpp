#include "LevelScreen.h"
#include "Game.h"
#include "AssetManager.h"
#include <stdlib.h>

LevelScreen::LevelScreen(Game* newGamePointer)
	: playerInstance(newGamePointer->GetWindow().getSize())
	, gamePointer(newGamePointer)
	, cactusInstance()
	, birdInstance()
	, cacti()
	, interval(0)
	, score(0)
	, gameFont(AssetManager::RequestFont("Assets/MainFont.ttf"))


{
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(50);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(500, 20);

	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.setVolume(25);
	gameMusic.play();

}

void LevelScreen::Input()
{
	if (playerInstance.GetAlive()) playerInstance.Input(gamePointer->GetWindow().getSize());
}

void LevelScreen::Update(sf::Time frameTime, sf::Vector2u screenSize)
{
	if (playerInstance.GetAlive())
	{

		score += frameTime.asSeconds();
		scoreText.setString("Score: " + std::to_string((int)score));//score update
		interval += frameTime.asSeconds();
		if (interval > 3)
		{
			AddEnemy();
		}
		for (int i = 0; i < cacti.size(); i++)
		{
			cacti[i].Update(frameTime, screenSize);
		}
		playerInstance.Update(frameTime, screenSize);
		for (int i = 0; i < cacti.size(); i++)
		{
			playerInstance.HandleSolidCollision(cacti[i].GetHitbox());
		}
	}
	else
	{
		gameMusic.stop();

	}
}


void LevelScreen::DrawTo(sf::RenderTarget& target)
{
	for (int i = 0; i < cacti.size(); i++)
	{
		cacti[i].DrawTo(target);
	}
	playerInstance.DrawTo(target);
	gamePointer->GetWindow().draw(scoreText);
}

void LevelScreen::AddEnemy()
{

	// Choose the type of platform to create
	int choiceMin = 0;
	int choiceMax = 100;
	int choice = rand() % (choiceMax - choiceMin) + choiceMin;
	int chanceMoving = 50;
	if (choice < 50)
	{
		EnemyFly newEnemy;
		cacti.push_back(newEnemy);
	}
	else
	{
		Enemy newEnemy;
		cacti.push_back(newEnemy);
	}
	interval = 0;
}
