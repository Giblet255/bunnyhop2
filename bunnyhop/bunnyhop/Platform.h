#pragma once
#include "SpriteObject.h"
class Platform :
	public SpriteObject
{
public:
	// Constructors / Destructors
	Platform();
	void SetPosition(sf::Vector2f newPosition);
};

