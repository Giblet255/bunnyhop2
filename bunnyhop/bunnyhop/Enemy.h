#pragma once
#include "SpriteObject.h"
#include <stdlib.h>
class Enemy :
    public SpriteObject
{
public:
    Enemy();
    Enemy(sf::Texture& newTexture, sf::Vector2f newPosition);
    virtual void Update(sf::Time frameTime, sf::Vector2u screenSize);

    //Setters


private:
    int velocity;
    sf::Vector2f position;
};
