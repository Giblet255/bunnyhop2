#pragma once
#include <SFML/Graphics.hpp>
class LevelScreen
{
public:
	LevelScreen();
	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);
};