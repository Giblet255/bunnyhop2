#include "Player.h"
#include "AssetManager.h"



Player::Player(sf::Vector2u screenSize)
	: AnimatingObjects(AssetManager::RequestTexture("Assets/Graphics/DinoSprite.png"), 100, 80, 8.0f)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, gravity(1000.0f)
	, isCrouching(false)
	, isAlive(true)
{
	AddClip("Run", 0, 1);
	AddClip("Jump", 4, 5);
	AddClip("Crouch", 2, 3);
	AddClip("Dead", 6, 6);
	PlayClip("Run", true);
	//position the player at the centre of the screen
	sf::Vector2f newPosition;
	newPosition.x = ((float)screenSize.x - sprite.getGlobalBounds().width) / 2.0f;
	newPosition.y = ((float)screenSize.y - sprite.getGlobalBounds().height) / 2.0f;
	sprite.setPosition(newPosition);

	deadBuffer.loadFromFile("Assets/Audio/death.wav");
	gameDead.setBuffer(deadBuffer);
	gameDead.setVolume(25);
}

void Player::Input(sf::Vector2u screenSize)
{

	// Player keybind input (x direction only)
	// Start by zeroing out player x velocity
	velocity.x = 0.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
	}
	if (sprite.getPosition().y == (2 * screenSize.y) / 3)
	{

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			velocity.y = speed;

			const float JUMP_VALUE = -600; // negative to go up. Adjust as needed.
			velocity.y = JUMP_VALUE;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		PlayClip("Crouch", true);
		isCrouching = true;
	}
	else if (sprite.getPosition().y > (2 * screenSize.y) / 3)
	{
		PlayClip("Jump", true);
	}
	else
	{
		PlayClip("Run", true);
		isCrouching = false;
	}

}

void Player::Update(sf::Time frameTime, sf::Vector2u screenSize)
{

	previousPosition = sprite.getPosition();
	// Calculate the new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	if (newPosition.y > (2 * screenSize.y) / 3)
	{
		newPosition.y = previousPosition.y;
		velocity.y = 0.0f;
	}
	// Move the player to the new position
	sprite.setPosition(newPosition);
	AnimatingObjects::Update(frameTime);
	//calculate the new velocity
	velocity.y = velocity.y + gravity * frameTime.asSeconds();
}

bool Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{

	//If we are colliding with a Platform...
	if (GetHitBox().intersects(otherHitbox))
	{
		isAlive = false;
		gameDead.play();
	}
	return isAlive;
}

sf::FloatRect Player::GetHitBox()
{
	sf::FloatRect oldHitbox = SpriteObject::GetHitbox();

	sf::FloatRect newHitbox = oldHitbox;
	if (isCrouching == true)
	{
		newHitbox.height = newHitbox.height / 2;
		newHitbox.top += oldHitbox.height * 1 / 2;
	}

	return newHitbox;
}

bool Player::GetAlive()
{
	return isAlive;
}