#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include "Player.h"
#include "Enemy.h"
#include "EnemyFly.h"


class Game;
class LevelScreen
{
public:
	LevelScreen(Game* newGamePointer);
	void Input();
	void Update(sf::Time frameTime, sf::Vector2u screenSize);
	void DrawTo(sf::RenderTarget& target);
	void AddEnemy();
private:
	Player playerInstance;
	Game* gamePointer;
	Enemy cactusInstance;
	EnemyFly birdInstance;
	std::vector<Enemy> cacti;
	float interval;
	float score;
	sf::Text scoreText;
	sf::Font gameFont;
	sf::Music gameMusic;


};

